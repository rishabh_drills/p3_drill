const data = require('./inventory');

const result = data.filter((card, index) => {
    const sum = card["card_number"].split("")
        .map((number) => parseInt(number))
        .filter((number, index) => index %2 == 0)
        .reduce((acc, number) => acc + number, 0);
        
    card.sum = sum;
    return card.sum % 2 != 0;
});

console.log(result);