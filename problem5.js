const data = require('./inventory');


const checkValidity = data.map((element) => ({
    ...element, validation: ''
    })).filter((user) => {
    let month = parseInt(user["issue_date"].split("/")[0]);
    if(month < 3 ){
        return user["validation"] = "Invalid";
    }else{
        return user["validation"] = "Valid"
    }
})

console.log(checkValidity);